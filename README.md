[![License](https://poser.pugx.org/bitandblack/typography/license)](https://packagist.org/packages/bitandblack/typography)
[![PHP version](https://badge.fury.io/ph/bitandblack%2Ftypography.svg)](https://badge.fury.io/ph/bitandblack%2Ftypography)
[![npm version](https://badge.fury.io/js/bitandblack-typography.svg)](https://badge.fury.io/js/bitandblack-typography)

# Bit&Black Typography

Helps writing good typography in SCSS.

## Installation

This library is made for the use with [NPM](https://www.npmjs.com/package/bitandblack-typography), [Yarn](https://yarnpkg.com/en/package/bitandblack-typography) and [Composer](https://packagist.org/packages/bitandblack/typography). Use one of them to add it to your project: 

### Node

`$ npm install bitandblack-typography` 

### Yarn 

`$ yarn add bitandblack-typography` 

### Composer 

`$ composer require bitandblack/typography`

## Usage

__Typography__ provides some mixins which will make it easy to improve the typography on your page.

### Balance ragged lines 

Use `@import balance-ragged-lines()` to get the width property. 

You can define that variable globally or by calling the mixin:

*   `$typo-balance-ragged-lines-value`. Default is `103%`. 

### Box decoration break

Use `@import box-decoration-break()` to get the box-decoration-break property. 

You can define that variable globally or by calling the mixin:

*   `$typo-box-decoration-break`. Default is `clone`. 

### Columns 

Use `@import columns()` to get all the column related properties. 

You can define those variables globally or by calling the mixin:

*   `$typo-column-count`. Default is `3`. 
*   `$typo-column-gap`. Default is `1rem`. 

### Color scheme 

Use `@import color-scheme-dark()` or `@import color-scheme-light()` to make a difference in your layout depending on the users color scheme setting. 

### Hanging punctuation 

Use `@import hanging-punctuation()` to get the hanging-punctuation property. 

You can define that variable globally or by calling the mixin:

*   `$typo-hanging-punctuation`. Default is `first last allow-end`. 

### Hyphens 

Use `@import hyphens()` to get all hyphen related properties. 

You can define those variables globally or by calling the mixin:

*   `$only-if-fully-supported`. Default is `false`.
*   `$typo-hyphens-limit-before`. Default is `3`. 
*   `$typo-hyphens-limit-after`. Default is `3`. 
*   `$typo-hyphens-limit-word`. Default is `6`. 
*   `$typo-hyphens-limit-lines`. Default is `3`. 
*   `$typo-hyphens-limit-last`. Default is `always`. 
*   `$typo-hyphens-limit-zone`. Default is `8%`. 

### Indent 

Use `@import indent()` to get all indent related properties. 

You can define those variables globally or by calling the mixin:

*   `$typo-indent-value`. Default is `1rem`. 
*   `$typo-indent-first-element`. Default is `false`. 

### Keep lines 

Use `@import keep-lines()` to get all properties to keep lines together. 

You can define those variables globally or by calling the mixin:

*   `$typo-orphans-value`. Default is `2`. 
*   `$typo-widows-value`. Default is `2`.

### Page break 

Use `@import page-break-before()`, `@import page-break-inside()` or `@import page-break-after()` to get the properties to keep elements together. 

You can define those variables globally or by calling the mixin:

*   `$typo-page-break-before`. Default is `avoid`. 
*   `$typo-page-break-inside`. Default is `avoid`. 
*   `$typo-page-break-after`. Default is `avoid`. 

## SCSS and CSS

If you don't have the possibility to use the `scss` files, you can use the pre-compiled `css` file in the `dist` folder. Please note that you lose the possibility to configure the stylesheets by your needs then.

## Help 

If you have any questions, feel free to contact us under `hello@bitandblack.com`.

Further information about Bit&Black can be found under [www.bitandblack.com](https://www.bitandblack.com).